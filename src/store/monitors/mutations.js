const getDefaultState = () => {
    return {
        monitors: null
    }
};

export default {
    setMonitors(state, payload) {
        state.monitors = payload
    },
    removeMonitor(state, id) {
        state.monitors.data = state.monitors.data.filter(monitor => monitor.id !== id)
    },
    clearState(state) {
        Object.assign(state, getDefaultState())
    }
}
