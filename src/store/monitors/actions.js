export default {
    createMonitor({ commit }, { payload }) {
        return this.$axios.post('/api/v1/monitors', payload).then((response) => {
            //
        })
    },
    getMonitors({ commit }, { params }) {
        return this.$axios.get('/api/v1/monitors', { params: params }).then((response) => {
            commit('setMonitors', response.data)
        })
    },
    checkMonitors({ commit }, { payload }) {
        return this.$axios.post('/api/v1/monitors/check', payload).then((response) => {
            //
        })
    },
    deleteMonitor({ commit }, { id }) {
        return this.$axios.delete(`api/v1/monitors/${id}`).then((response) => {
            commit('removeMonitor', id)
        })
    }
}
