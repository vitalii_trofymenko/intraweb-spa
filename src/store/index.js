import Vue from 'vue';
import Vuex from 'vuex';
import auth from './auth';
import monitors from './monitors';
import { axios } from '../plugins/axios';

Vue.use(Vuex);

const axiosPlugin = store => {
    store.$axios = axios
};

export default new Vuex.Store({
    modules: {
        auth: auth,
        monitors: monitors
    },
    plugins: [
        axiosPlugin
    ]
})
