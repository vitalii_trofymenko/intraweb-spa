const getDefaultState = () => {
    return {
        loggedIn: false,
        user: null
    }
};

export default {
    setUser(state, payload) {
        state.loggedIn = Boolean(payload);
        state.user = payload
    },
    clearAuth(state) {
        Object.assign(state, getDefaultState())
    }
}
