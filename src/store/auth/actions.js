export default {
    setCsrfCookie() {
        return this.$axios.get('/airlock/csrf-cookie').then((response) => {
            //
        })
    },
    login({ dispatch }, { payload }) {
        return this.$axios.post('/api/v1/auth/login', payload).then((response) => {
            return dispatch('getAuthUser')
        })
    },
    getAuthUser({ commit }) {
        return this.$axios.get('/api/v1/auth/user').then((response) => {
            commit('setUser', response.data.data)
        })
    },
    logout({ commit }) {
        return this.$axios.post('/api/v1/auth/logout').then((response) => {
            commit('clearAuth')
        })
    }
}
