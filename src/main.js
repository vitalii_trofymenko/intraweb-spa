import Vue from 'vue';
import App from './App.vue';
import './assets/css/main.css';
import router from './router';
import store from './store';
import outsideClickable from './directives/outsideClickable';

require('./plugins/vee-validate');

Vue.config.productionTip = false;

Vue.directive('outside-clickable', outsideClickable);

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
