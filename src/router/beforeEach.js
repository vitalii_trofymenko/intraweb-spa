import store from '../store'

export default (to, from, next) => {
    if (to.meta.requiresAuth) {
        if (!store.state.auth.loggedIn) {
            store.dispatch('auth/getAuthUser').then(() => {
                next()
            }).catch(() => {
                next({
                    name: 'auth-login'
                })
            })
        } else {
            next()
        }
    } else {
        if (store.state.auth.loggedIn) {
            next({
                name: 'home'
            })
        } else {
            next()
        }
    }
}
