import Vue from 'vue'
import VueRouter from 'vue-router'
import beforeEach from './beforeEach'

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'home',
        component: () => import('../views/index'),
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/auth/login',
        name: 'auth-login',
        component: () => import('../views/auth/login'),
        meta: {
            requiresAuth: false
        }
    }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.beforeEach(beforeEach);

export default router
